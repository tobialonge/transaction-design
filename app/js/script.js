$(document).ready(function () {
var container = $('#page'),
    cover = $('.cover'),
    navToggleBtn = container.find('.nav-toggle-btn');

    navToggleBtn.on('click', function(e) {
        container.toggleClass('active-nav');
        cover.toggleClass('active-nav');
        e.preventDefault();
    });
    cover.on('click', function(e) {
        container.toggleClass('active-nav');
        cover.toggleClass('active-nav');
        e.preventDefault();
    });
});