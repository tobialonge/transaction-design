var gulp = require('gulp'),
    gutil = require('gulp-util'), 
    sass = require('gulp-sass');

gulp.task('default', function() {
  return gutil.log('Gulp is running!')
});

gulp.task('sass', function() {
  return gulp.src('app/scss/**/*.scss')
    .pipe(sass())
    .pipe(gulp.dest('app/css'))
})

gulp.task('watch', function(){
  gulp.watch('app/scss/**/*.scss', ['sass']); 
  // Other watchers
})